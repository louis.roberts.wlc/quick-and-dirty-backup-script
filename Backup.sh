#!/bin/bash
logfile="/home/louis/Desktop/backup.log" #Where the logfile should be sent.
date="$(date +"%d_%m_%Y_%I:%M")" #Gets current date and time. Example -> 14_11_2022_08:18
backupdrive="/run/media/louis/HomeBackup/"
backupdir="$backupdrive$date" #Example: [/run/media/louis/HomeBackup]/[14_11_2022_08]

#holds the exclude flags for the rsync command below
exclude="--exclude "/louis/.local/share/Steam/*" --exclude="/louis/Templates/*" --exclude="/louis/.PlayOnLinux/*" --exclude="/louis/.cache/*" --exclude="/louis/.local/share/GSC Game World/*" --exclude="/guest/""

{
if [[ -e "$backupdrive" ]]; then
    cd $backupdrive
    mkdir $date
    rsync -aAXHv $exclude /home/ $backupdir
    pacman -Q > /run/media/louis/HomeBackup/packages.txt #Drop a list of currently installed packages
    exit 1
else # Notify if backup drive isn't mounted and quit, don't try to backup.
    echo "Backup drive isn't mounted"
    exit 0
fi
} | tee $logfile #log the rsync outputs